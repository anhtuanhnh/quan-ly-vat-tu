import React, {Component} from 'react'
import { Route, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class SideBar extends Component {
    constructor(){
        super();
        this.state = {
            isShowMenu: false
        };
        this.toggleMenu = this.toggleMenu.bind(this)
    }
    toggleMenu(){
        this.setState({isShowMenu: !this.state.isShowMenu})
    }
    render(){
        const {email, isAuthenticated } = this.props;

        return(
            <aside className="main-sidebar">
                <section className="sidebar">
                    <div className="user-panel">
                        <div className="pull-left image">
                            <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" className="img-circle" alt="User Image" />
                        </div>
                        <div className="pull-left info">
                            <p>{email}</p>
                            <a href="#"><i className="fa fa-circle text-success"/> Online</a>
                        </div>
                    </div>
                    <ul className="sidebar-menu tree" data-widget="tree">
                        <li className="header">MAIN NAVIGATION</li>
                        <li className="active treeview menu-open">
                            <Link to="/app">
                                <i className="fa fa-dashboard"/> <span>Bảng điều khiển</span>
                            </Link>
                        </li>
                        <li className="treeview menu-open">
                            <Link to="/devices">
                                <i className="fa fa-files-o"/>
                                <span>Thiết bị</span>
                                <span className="pull-right-container">
                                    <span className="label label-primary pull-right">4</span>
                                </span>
                            </Link>
                            <ul className="treeview-menu">
                                <li><a href="pages/layout/top-nav.html"><i className="fa fa-circle-o"/> Top Navigation</a></li>
                                <li><a href="pages/layout/boxed.html"><i className="fa fa-circle-o"/> Boxed</a></li>
                                <li><a href="pages/layout/fixed.html"><i className="fa fa-circle-o"/> Fixed</a></li>
                                <li><a href="pages/layout/collapsed-sidebar.html"><i className="fa fa-circle-o"/> Collapsed Sidebar</a></li>
                            </ul>
                        </li>
                        <li>
                            <Link to="/borrow-device">
                                <i className="fa fa-th"/> <span>Quản lý đơn mượn</span>
                                <span className="pull-right-container">
                                    <small className="label pull-right bg-green">new</small>
                                </span>
                            </Link>
                        </li>

                        <li>
                            <Link to="/manage-devices">
                                <i className="fa fa-calendar"/> <span>Quản lý thiết bị</span>
                                <span className="pull-right-container">
                                    <small className="label pull-right bg-red">3</small>
                                </span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/manage-users">
                                <i className="fa fa-envelope"/> <span>Quản lý người dùng</span>
                                <span className="pull-right-container">
                                    <small className="label pull-right bg-red">5</small>
                                </span>
                            </Link>
                        </li>

                    </ul>
                </section>
            </aside>
        )
    }
}


const mapStateToProps = state =>({
    isAuthenticated:  state.userReducer.isAuthenticated,
    email:  state.userReducer.email,
})

const mapDispatchToProps = dispatch => bindActionCreators({

}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SideBar)

SideBar.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired,
    email: PropTypes.string.isRequired,
}