import firebase from '../firebase.js'
import {get} from './api'
import * as types from '../constants/ActionType'


export const handleLoginSuccess = (results) => ({
    type: types.HANDLE_LOGIN_SUCCESS,
    results
})
export const handleLoginFail = (results) => ({
    type: types.HANDLE_LOGIN_FAIL,
    results
})

export const handleFetchProfile = (email) => ({
    type: types.HANDLE_FETCH_PROFILE,
    email
})

export const handleCheckLogin = () =>{
    return (dispatch) =>{
        const currentUser = firebase.auth().onAuthStateChanged(function(user) {
            window.user = user; // user is undefined if no user signed in;
            console.log(user)

            if(user){
                dispatch(handleLoginSuccess());
                dispatch(handleFetchProfile(user.email));
            }
        });
    }
}
export const handleLogout = () => {
    return (dispatch) =>{
        firebase.auth().signOut()
            .then(function () {
                dispatch(handleLoginFail());
            })
            .catch(function (err) {
                // Handle errors
            });
    }
}
export const handleLogin = (email, password, remember, redirect) =>{
    console.log(email, password, remember)
    return function (dispatch) {
        if(remember){
            firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
                .then(function() {
                    // Existing and future Auth states are now persisted in the current
                    // session only. Closing the window would clear any existing state even
                    // if a user forgets to sign out.
                    // ...
                    // New sign-in will be persisted with session persistence.
                    return firebase.auth().signInWithEmailAndPassword(email, password).then((res) => {
                        console.log(res);
                        localStorage.setItem('uid', res.uid);
                        dispatch(handleFetchProfile(res.email));
                        dispatch(handleLoginSuccess());
                        redirect.push('/app')
                    });
                })
                .catch(function(error) {
                    // Handle Errors here.
                    const errorCode = error.code;
                    const errorMessage = error.message;
                });
        } else {
            firebase.auth()
                .signInWithEmailAndPassword(email, password)
                .then((res) => {
                    console.log(res);
                    localStorage.setItem('uid', res.uid);
                    dispatch(handleFetchProfile(res.email));
                    dispatch(handleLoginSuccess());
                    redirect.push('/app')
                })
                .catch((e) => {
                    // cb(false, e);
                });
        }

    };

}
