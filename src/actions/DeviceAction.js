import firebase from '../firebase.js'
import {get} from './api'
import * as types from '../constants/ActionType'



export const fetchDevicesSuccess = (devices) => ({
    type: types.FETCH_DEVICES_SUCCESS,
    devices
})


export const fetchDataUsers = () =>{
    return function (distpatch) {
        get('devices', (res)=>{
            distpatch(fetchDevicesSuccess(res))
        })
    }
}