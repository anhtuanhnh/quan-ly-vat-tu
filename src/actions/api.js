import firebase from '../firebase.js'

export const post = (data, node, cb) =>{
    const id = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 9);
    const dataNew = data;
    dataNew.id = id;
    firebase.database()
        .ref(`node/${id}`)
        .set(dataNew)
        .then((res) => {
            console.log(res);
            cb(true);
        });
}
export const get = (node, cb) =>{
    firebase.database().ref(`/${node}`).on('value', (snapshot) => {
        setTimeout(() => {
            const posts = snapshot.val() || [];
            console.log(Object.values(posts));
            cb(Object.values(posts))
        }, 0);
    });
}