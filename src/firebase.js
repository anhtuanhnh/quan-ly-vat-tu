import firebase from 'firebase'
const config = {
    apiKey: "AIzaSyC6A981LMSgvB-fl78KuxiWh2X5tOD9Fx4",
    authDomain: "erp-software-8941a.firebaseapp.com",
    databaseURL: "https://erp-software-8941a.firebaseio.com",
    projectId: "erp-software-8941a",
    storageBucket: "erp-software-8941a.appspot.com",
    messagingSenderId: "46041828504"
};
firebase.initializeApp(config);
export default firebase;