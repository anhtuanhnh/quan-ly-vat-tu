import * as types from '../constants/ActionType'

const initialState = {
    isAuthenticated: false,
    email: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.HANDLE_LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true
            }
        case types.HANDLE_LOGIN_FAIL:
            return {
                ...state,
                isAuthenticated: false
            }
        case types.HANDLE_FETCH_PROFILE:
            return {
                ...state,
                email: action.email
            }
        default:
            return state
    }
}