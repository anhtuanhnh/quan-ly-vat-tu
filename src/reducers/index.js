import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import userReducer from './UserReducer'
import deviceReducer from './DeviceReducer'

export default combineReducers({
    router: routerReducer,
    userReducer,
    deviceReducer
})
