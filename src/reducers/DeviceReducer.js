import * as types from '../constants/ActionType'

const initialState = {
    devices: []
}

export default (state = initialState, action) => {
    switch (action.type) {
        case types.FETCH_DEVICES_SUCCESS:
            return {
                ...state,
                devices: action.devices
            }
        default:
            return state
    }
}