import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { Route, Link } from 'react-router-dom'
import PropTypes from 'prop-types'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    fetchDataUsers
} from '../../actions/DeviceAction'
class Devices extends Component{
    constructor(){
        super();
        this.state = {

        }

    }
    componentWillMount(){
        this.props.fetchDataUsers()
    }
    componentWillReceiveProps(nextProps){
        console.log(nextProps)
    }

    render(){
        const { isLoadding, inputValue, typeSearch, isListeningVoice, isSearchingVoice, dataResult } = this.state;
        console.log(this.props.devices)
        return(
            <div className="content-wrapper" style={{minHeight: '960px'}}>
                <section className="content-header">
                    <h1>
                        Thiết bị
                        <small>Control panel</small>
                    </h1>
                    <ol className="breadcrumb">
                        <li><Link to="/app"><i className="fa fa-dashboard"></i> Trang chủ</Link></li>
                        <li className="active">Thiết bị</li>
                    </ol>
                </section>

                <section className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box">
                                <div className="box-body">
                                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap"><div className="row"><div className="col-sm-6"><div className="dataTables_length" id="example1_length"><label>Show <select name="example1_length" aria-controls="example1" className="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div className="col-sm-6"><div id="example1_filter" className="dataTables_filter"><label>Search:<input type="search" className="form-control input-sm" placeholder="" aria-controls="example1"/></label></div></div></div><div className="row"><div className="col-sm-12"><table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                        <tr role="row">
                                            <th className="sorting_asc" >Rendering engine</th>
                                            <th className="sorting" >Browser</th>
                                            <th className="sorting" >Platform(s)</th>
                                            <th className="sorting" >Engine version</th>
                                            <th className="sorting">CSS grade</th></tr>
                                        </thead>
                                        <tbody>
                                        <tr role="row" className="odd">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Firefox 1.0</td>
                                            <td>Win 98+ / OSX.2+</td>
                                            <td>1.7</td>
                                            <td>A</td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th >Rendering engine</th>
                                            <th >Browser</th>
                                            <th >Platform(s)</th>
                                            <th >Engine version</th>
                                            <th >CSS grade</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    </div>
                                    </div>
                                        <div className="row">
                                            <div className="col-sm-5">
                                                <div className="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                            </div>
                                            <div className="col-sm-7">
                                                <div className="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                                    <ul className="pagination">
                                                        <li className="paginate_button previous disabled" id="example1_previous">
                                                            <a href="#" aria-controls="example1" data-dt-idx="0" >Previous</a>
                                                        </li>
                                                        <li className="paginate_button active">
                                                            <a href="#" aria-controls="example1" data-dt-idx="1" >1</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" >2</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" >3</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" >4</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" >5</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" >6</a>
                                                        </li>
                                                        <li className="paginate_button next" id="example1_next">
                                                            <a href="#" aria-controls="example1" data-dt-idx="7" >Next</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    devices: state.devices
})

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchDataUsers,
    changePage: () => push('/search-result-us')
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Devices)
Devices.propTypes = {
    devices: PropTypes.array,
}