import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { Route, Link } from 'react-router-dom'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    handleCheckLogin
} from '../../../actions/UserAction'
class ManageUsers extends Component{
    constructor(){
        super();
        this.state = {

        }

    }
    componentDidMount(){


    }


    render(){
        const { isLoadding, inputValue, typeSearch, isListeningVoice, isSearchingVoice, dataResult } = this.state;
        return(
            <div className="content-wrapper" style={{minHeight: '960px'}}>
                <section className="content-header">
                    <h1>
                        Quản lý người dùng
                        <small>Control panel</small>
                    </h1>
                    <ol className="breadcrumb">
                        <li><Link to="/app"><i className="fa fa-dashboard"></i> Trang chủ</Link></li>
                        <li className="active">Quản lý người dùng</li>
                    </ol>
                </section>

                <section className="content">
                    <div className="row">
                        <div className="col-md-12">
                            <div className="box">
                                <div className="box-body">
                                    <div id="example1_wrapper" className="dataTables_wrapper form-inline dt-bootstrap"><div className="row"><div className="col-sm-6"><div className="dataTables_length" id="example1_length"><label>Show <select name="example1_length" aria-controls="example1" className="form-control input-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div className="col-sm-6"><div id="example1_filter" className="dataTables_filter"><label>Search:<input type="search" className="form-control input-sm" placeholder="" aria-controls="example1"/></label></div></div></div><div className="row"><div className="col-sm-12"><table id="example1" className="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                        <tr role="row"><th className="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" >Rendering engine</th><th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" >Browser</th><th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" >Platform(s)</th><th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" >Engine version</th><th className="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" >CSS grade</th></tr>
                                        </thead>
                                        <tbody>
                                        <tr role="row" className="odd">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Firefox 1.0</td>
                                            <td>Win 98+ / OSX.2+</td>
                                            <td>1.7</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="even">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Firefox 1.5</td>
                                            <td>Win 98+ / OSX.2+</td>
                                            <td>1.8</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="odd">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Firefox 2.0</td>
                                            <td>Win 98+ / OSX.2+</td>
                                            <td>1.8</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="even">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Firefox 3.0</td>
                                            <td>Win 2k+ / OSX.3+</td>
                                            <td>1.9</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="odd">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Camino 1.0</td>
                                            <td>OSX.2+</td>
                                            <td>1.8</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="even">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Camino 1.5</td>
                                            <td>OSX.3+</td>
                                            <td>1.8</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="odd">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Netscape 7.2</td>
                                            <td>Win 95+ / Mac OS 8.6-9.2</td>
                                            <td>1.7</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="even">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Netscape Browser 8</td>
                                            <td>Win 98SE+</td>
                                            <td>1.7</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="odd">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Netscape Navigator 9</td>
                                            <td>Win 98+ / OSX.2+</td>
                                            <td>1.8</td>
                                            <td>A</td>
                                        </tr><tr role="row" className="even">
                                            <td className="sorting_1">Gecko</td>
                                            <td>Mozilla 1.0</td>
                                            <td>Win 95+ / OSX.1+</td>
                                            <td>1</td>
                                            <td>A</td>
                                        </tr></tbody>
                                        <tfoot>
                                        <tr>
                                            <th rowspan="1" colspan="1">Rendering engine</th>
                                            <th rowspan="1" colspan="1">Browser</th>
                                            <th rowspan="1" colspan="1">Platform(s)</th>
                                            <th rowspan="1" colspan="1">Engine version</th>
                                            <th rowspan="1" colspan="1">CSS grade</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                    </div>
                                    </div>
                                        <div className="row">
                                            <div className="col-sm-5">
                                                <div className="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                            </div>
                                            <div className="col-sm-7">
                                                <div className="dataTables_paginate paging_simple_numbers" id="example1_paginate">
                                                    <ul className="pagination">
                                                        <li className="paginate_button previous disabled" id="example1_previous">
                                                            <a href="#" aria-controls="example1" data-dt-idx="0" tabindex="0">Previous</a>
                                                        </li>
                                                        <li className="paginate_button active">
                                                            <a href="#" aria-controls="example1" data-dt-idx="1" tabindex="0">1</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="2" tabindex="0">2</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="3" tabindex="0">3</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="4" tabindex="0">4</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="5" tabindex="0">5</a>
                                                        </li>
                                                        <li className="paginate_button "><a href="#" aria-controls="example1" data-dt-idx="6" tabindex="0">6</a>
                                                        </li>
                                                        <li className="paginate_button next" id="example1_next">
                                                            <a href="#" aria-controls="example1" data-dt-idx="7" tabindex="0">Next</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
    handleCheckLogin,
    changePage: () => push('/search-result-us')
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ManageUsers)
