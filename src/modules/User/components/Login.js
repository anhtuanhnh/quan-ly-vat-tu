import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    handleLogin
} from '../../../actions/UserAction'
import Header from '../../../components/Header/Header'
class Login extends Component{
    constructor(){
        super();
        this.state = {

        }
        this.onSubmit = this.onSubmit.bind(this)

    }
    onSubmit(event) {
        event.preventDefault()
        const password = this.refs.password.value
        const email = this.refs.email.value
        const rememberMe = this.refs.rememberMe.checked
        this.props.handleLogin(email,password,rememberMe, this.props.history)
    }
    render(){
        const { isLoadding, inputValue, typeSearch, isListeningVoice, isSearchingVoice, dataResult } = this.state;
        return(
            <div className="page-wrapper">
                <div id="page-content">
                    <div className="container">
                        <div className="form-box" >
                            <div className="form-content width-400px">
                                <section className="page-title">
                                    <h2>Login</h2>
                                </section>
                                <section>
                                    <form className="form inputs-underline" onSubmit={(event) => this.onSubmit(event)}>
                                        <div className="form-group">
                                            <label htmlFor="email">Email</label>

                                            <input type="text" className="form-control" ref="email" id="email"  />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="password">Password</label>
                                            <input type="password" ref="password" className="form-control" name="password" id="password" placeholder="******"  />
                                        </div>
                                        <div className="checkbox">
                                            <label>
                                                <input ref="rememberMe" type="checkbox"/>
                                                Remember me
                                            </label>
                                        </div>
                                        <div className="form-group center">
                                            <button type="submit"  className="btn btn-primary btn-rounded width-100">
                                                Login
                                            </button>
                                        </div>
                                    </form>


                                </section>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="bg-login"/>
            </div>
        )
    }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
    handleLogin,
    changePage: () => push('/search-result-us')
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Login)
