import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { Route, Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
    handleCheckLogin
} from '../../actions/UserAction'
import Header from '../../components/Header/Header'
import './home.css'
class Home extends Component{
    constructor(){
        super();
        this.state = {

        }

    }
    componentWillMount(){
        this.props.history.push('/app')
    }


    render(){
        const { isLoadding, inputValue, typeSearch, isListeningVoice, isSearchingVoice, dataResult } = this.state;
        return(
           <div className="wrapper">
               <Link to="/app">Get start</Link>
           </div>
        )
    }
}

const mapStateToProps = state => ({
})

const mapDispatchToProps = dispatch => bindActionCreators({
    handleCheckLogin,
  changePage: () => push('/search-result-us')
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
