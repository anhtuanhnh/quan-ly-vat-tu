import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import '../../../../node_modules/admin-lte/dist/css/AdminLTE.css'
import '../../../../node_modules/admin-lte/dist/css/skins/_all-skins.css'
import '../../../assets/css/bootstrap.min.css'
import '../../../assets/css/dataTables.bootstrap.min.css'
import '../../../assets/css/style.css'
import '../../../assets/css/font-awesome.min.css'
import '../../../assets/css/ionicons.min.css'
import CoreRouter from './CoreRouter'
import { BrowserRouter as Router } from 'react-router-dom'

import {
    handleCheckLogin,
} from '../../../actions/UserAction'
class App extends Component {

	constructor(props) {
		super(props);
		this.state = {
            isAuthenticated: false
		}
	}

	doVerify() {
		// if (this.state.isAuthenticated){
		// 	this.props.verifyToken(this.state.token)
		// }
	}

	componentWillMount() {
		// let token = sessionStorage.getItem("token")
		// if (token){
		// 	this.setState({
		// 		isAuthenticated: true,
		// 		token: token
		// 	})
		// }
		this.props.handleCheckLogin()
	}

	render() {
		this.doVerify()
		return (
			<Router>
				<CoreRouter isAuthenticated={this.props.isAuthenticated}/>
			</Router>
		)
	}
}
const mapStateToProps = state => ({
    isAuthenticated:  state.userReducer.isAuthenticated
})

const mapDispatchToProps = dispatch => bindActionCreators({
    handleCheckLogin,
}, dispatch)

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)

App.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired,
}