import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class ErrorNotFound extends Component {
    static contextTypes = {
      router: PropTypes.object.isRequired,
    }

    componentDidMount() {
      this._interval = setInterval(() => {
        this.context.router.history.push('/');
      }, 3000);
    }

    componentWillUnmount() {
      clearInterval(this._interval);
    }

    render() {
      return (
          <div className="container" style={{paddingTop: 50}}>
            <section className="page-title center error">
              <h1>404</h1>
              <h2>Lỗi</h2>
              <p>Địa chỉ này không tồn tại!</p>
            </section>
          </div>
      );
    }
}
