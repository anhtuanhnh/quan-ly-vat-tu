import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Route, Switch } from 'react-router-dom'
import { publicLink, privateLink, unAuthLink } from '../routers/RouterConfig'
import PrivateRouter from '../routers/PrivateRouter'
import UnAuthRouter from '../routers/UnAuthRouter'
import ErrorNotFound from './ErrorNotFound'
import SideBar from '../../../components/SideBar/SideBar'
import Header from '../../../components/Header/Header'

export default class CoreRouter extends Component {

    render() {
        let isAuthenticated = this.props.isAuthenticated
        return (
            <Switch>
                {publicLink.map((route, index) => (
                    <Route
                        key={index}
                        path={route.path}
                        exact={route.exact}
                        component={route.component}
                    />
                ))}
                {unAuthLink.map((route, index) => (
                    <UnAuthRouter
                        key={index}
                        path={route.path}
                        isAuthenticated={isAuthenticated}
                        component={route.component}
                    />
                ))}
                <div className="wrapper">
                    <Header />
                    <SideBar/>
                    {
                        privateLink.map((route, index) => (
                            <PrivateRouter
                                key={index}
                                path={route.path}
                                isAuthenticated={isAuthenticated}
                                component={route.component}
                            />
                        ))
                    }
                </div>
                    <Route component={ErrorNotFound} />
            </Switch>
        )
    }
}

CoreRouter.propTypes = {
	isAuthenticated: PropTypes.bool.isRequired
}