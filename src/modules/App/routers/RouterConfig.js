import Login from '../../User/components/Login'
import Home from '../../Home/Home'
import Dashboard from '../../Dashboard/Dashboard'
import Devices from '../../Devices/Devices'
import BorrowDevice from '../../BorrowDevice/BorrowDevice'
import ManageDevices from '../../ManageDevices/ManageDevices'
import ManageUsers from '../../User/components/ManageUsers'

export const publicLink = [
    {
        path: '/',
        exact: true,
        component: Home
    }
]

export const privateLink = [
    {
        path: '/app',
        component: Dashboard
    },
    {
        path: '/devices',
        component: Devices
    },
    {
        path: '/borrow-device',
        component: BorrowDevice
    },
    {
        path: '/manage-devices',
        component: ManageDevices
    },
    {
        path: '/manage-users',
        component: ManageUsers
    }
]

export const unAuthLink = [
    {
        path: '/login',
        component: Login
    },
]