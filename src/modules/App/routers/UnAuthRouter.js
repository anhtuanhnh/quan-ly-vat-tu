import React from 'react'
import { Route, Redirect } from 'react-router-dom'

const UnAuthRouter = ({component: Component, isAuthenticated, ...rest}) => (
    <Route {...rest} render={props => (!isAuthenticated ? (
        <Component {...props} />
        ) : (
            <Redirect to={{
                pathname: '/app'
            }}/>
        )
    )}/>
)

export default UnAuthRouter